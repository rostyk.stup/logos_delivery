package com.logos.lesson_start.controller;

import com.logos.lesson_start.domain.Shop;
import com.logos.lesson_start.dto.request.PaginationRequestDTO;
import com.logos.lesson_start.dto.request.ShopRequestDTO;
import com.logos.lesson_start.dto.response.PageResponse;
import com.logos.lesson_start.dto.response.ShopResponseDTO;
import com.logos.lesson_start.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/shops")
public class ShopController {

	@Autowired
	private ShopService shopService;

	@PostMapping
	private ShopResponseDTO create(@RequestBody ShopRequestDTO shop) {
		return new ShopResponseDTO(shopService.save(shop));
	}

	@PutMapping("/{id}")
	private ShopResponseDTO update(@RequestBody ShopRequestDTO shop, @PathVariable("id") Long id) {
		return new ShopResponseDTO(shopService.update(shop, id));
	}

	@GetMapping("/{id}")
	private ShopResponseDTO get(@PathVariable("id") Long id) {
		return new ShopResponseDTO(shopService.getById(id));
	}

	@DeleteMapping("/{id}")
	private void delete(@PathVariable("id") Long id) {
		shopService.delete(id);
	}

	@GetMapping()
	private List<ShopResponseDTO> getAll() {
		return shopService.getAll()
				.stream()
				.map(ShopResponseDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/page")
	private PageResponse<ShopResponseDTO> getPage(PaginationRequestDTO paginationRequestDTO) {
		Page<Shop> page = shopService.getPage(paginationRequestDTO);
		return new PageResponse<>(
				page.get().map(ShopResponseDTO::new).collect(Collectors.toList()),
				page.getTotalElements(),
				page.getTotalPages()
		);
	}

	@GetMapping("/category/{categoryId}")
	private List<ShopResponseDTO> getAllByCategory(@PathVariable("categoryId") Long id) {
		return shopService.getAllByCategory(id)
				.stream()
				.map(ShopResponseDTO::new)
				.collect(Collectors.toList());
	}



}
