package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.Cart;

public interface CartService {

	void addItem(Long cartId, Long itemId);

	Cart getById(Long id);

	Cart getByCurrentUser();

}
