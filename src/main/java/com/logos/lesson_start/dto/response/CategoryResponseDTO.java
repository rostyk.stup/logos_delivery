package com.logos.lesson_start.dto.response;

import com.logos.lesson_start.domain.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryResponseDTO {

	private Long id;
	private String name;

	public CategoryResponseDTO(Category category) {
		this.id = category.getId();
		this.name = category.getName();
	}
}
