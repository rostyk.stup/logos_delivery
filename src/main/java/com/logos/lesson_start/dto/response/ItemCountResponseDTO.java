package com.logos.lesson_start.dto.response;

import com.logos.lesson_start.domain.Item;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemCountResponseDTO {

	private ItemResponseDTO item;

	private Integer count;

	public ItemCountResponseDTO(Item item, Integer count) {
		this.item = new ItemResponseDTO(item);
		this.count = count;
	}
}
