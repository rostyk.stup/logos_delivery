package com.logos.lesson_start.dto.response;

import com.logos.lesson_start.domain.Cart;
import com.logos.lesson_start.domain.Item;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;

@Getter
@Setter
public class CartResponseDTO {

	private Integer sum = 0;

	private List<ItemCountResponseDTO> items = new ArrayList<>();

	public CartResponseDTO(Cart cart) {
		items = cart.getItemsCountMap()
				.entrySet()
				.stream()
				.peek(e -> sum += e.getKey().getPrice() * e.getValue())
				.map(e -> new ItemCountResponseDTO(e.getKey(), e.getValue()))
				.collect(Collectors.toList());

//		items = cart.getItemsCountMap()
//				.entrySet()
//				.stream()
//				// peek - проміжна операція яка ніяк не модифікує Stream,
//				// але дозволяє виконати певні операції з його елементами
//				// в даному випадку 'e' - екземляр класу Map.Entry<Item, Integer>
//				// має методи getKey() i getValue() як було сказано на лекції, це як мапа лише з 1 записом
//				.peek(e -> sum += e.getKey().getPrice() * e.getValue())
//				// map - проміжна операція в результаті якої елементи мають "перемапитись"
//				// якось модифікуватись (переважно в екзампляри іншого класу)
//				// тут ми вказуємо що "e" потрібно перетворити у спосіб:
//				// передати його key i value у контруктор ItemCountResponseDTO
//				// після цієї операції маємо Stream з ItemCountResponseDTO
//				.map(e -> new ItemCountResponseDTO(e.getKey(), e.getValue()))
//				.collect(Collectors.toList());
	}
}
