package com.logos.lesson_start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LessonStartApplication {
	private static final String TEST = "CONST";
	private static final String TEST_C = "CONST_C";

	public static void main(String[] args) {
		SpringApplication.run(LessonStartApplication.class, args);
	}

	// second commit
	// add to git -> commit -> change files -> add to git -> commit -> push
	// -----*--------* local

	// -----*--------*-----*-------*---------      remote
	//                       \          /
	//                        \____*_*_/     test-conflict

}
