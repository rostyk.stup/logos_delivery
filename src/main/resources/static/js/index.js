import {httpGet, httpDelete, httpPut, httpPost} from './http-tools.js'
import {appendHeader} from './header.js'


document.addEventListener('DOMContentLoaded', function () {
    appendHeader().then(r => {
        loadShops();
        // let request = httpGet("http://localhost:8080/carts");
        // console.log(request.response);
    });
});

// documentation + git
// sql join

const loadShops = () => {
    let request = httpGet("http://localhost:8080/shops");
    let response = JSON.parse(request.response);

    if (request.status === 200) {
        for (let shop of response) {
            appendShopToContainer(shop);
        }
    }
}

const appendShopToContainer = (shop) => {
    let container = document.getElementById('shops-container');
    container.innerHTML = container.innerHTML +
        `<div class="shop-plate">
            <div class="shop-plate-name">${shop.name}</div>
        </div>`
}